/// <reference types="vite/client" />
/* 全局声明（declare）Vue模块 */
declare module '*.vue' {
    import { Component } from 'vue'
    const component: Component
    export default component
}
