import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import TodoListHome from 'todolist/src/App.vue';
import CartHome from 'cart/src/App.vue';

const routes: Array<RouteRecordRaw> = [
    {
        path: '/todos',
        component: TodoListHome
    },
    {
        path: '/cart',
        component: CartHome
    },
    // 其他路由...
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;